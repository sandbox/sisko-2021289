<?php

  /** @file Webform Component information for a phone number field type */
  /**
   * Specify the default properties of a component.
   * @return
   *   An array defining the default structure of a component.
   */
      function _webform_defaults_postcodeanywhere() {
        return array(
          'name'      => '',
          'form_key'  => NULL,
          'mandatory' => 0,
          'pid'       => 0,
          'weight'    => 0,
          'extra'     => array(
            'title_display'              => 0,
            'private'                    => FALSE,
            'attributes'                 => array(),
            'description'                => '',
            'collapsible'                => 0,
            'collapsed'                  => 0,
            #'country'                    => 'ca',
            #'phone_country_code'         => 0,
            #'phone_default_country_code' => 1,
            #'phone_int_max_length'       => 15,
            #'ca_test_separator'         => '-',
            #'ca_test_parentheses'       => 1,
          ),
        );
      }

/*function _webform_defaults_fieldset() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'title_display' => 0,
      'collapsible' => 0,
      'collapsed' => 0,
      'description' => '',
      'private' => FALSE,
    ),
  );
}*/