<?php

/*************************************
 * adds textfield webform component
 *************************************/
  function _create_postcodeanywhere_textfield_component($nid, $new_component, $cid, $pid, $parent_weight){
    $node = node_load($nid);

    #drupal_set_message("WORKING ON: NID={$nid}, COMPNENT={$new_component}, CID={$cid}, PID={$pid}", 'warning');

    $processed_name  = str_replace(' ', '_', strtolower($new_component));
    #drupal_set_message("Attempting to insert into: {$index}");
    // Create the webform components array. Not sure if we need all these
    // values, but let's be sure.
    $component = array(
      'cid' =>  (int)$cid,
      'pid' =>  (int)$pid,
      'nid' =>  (int)$node->nid,
      // I don't trust the admin to make a key based on input :)
      'form_key' => $processed_name,
      'name' => $new_component,
      // I want all lines to be numeric type component.
      'type' => 'textfield',
      'value' => '',
      'extra' => array(),
      'mandatory' => '0',
      'weight' => $parent_weight,# 0,
      'page_num' => 1,
    );

    #return $component;

    #dvm($component);
    webform_component_insert($component);
    #array_push($node->webform['components'], $component);
    #node_save($node);

    drupal_set_message("{$new_component} component successfully created in {$node->title}");

    #return $component;

  }