(function ($) {

    jQuery.extend({
        testing: function(){
            alert('TEST function responding!');
        }
    });

    //$('#edit-submitted-personal-selected-address').html('testing ... testing');

    jQuery.extend({
    //$('input#btnFind').click(
    //$('input.btnFind').click(
        initiate: function($postcode){

            //alert( $('#postcode').val() );
        //function PostcodeAnywhere_Interactive_Find_v1_10Begin(Key, SearchTerm, PreferredLanguage, Filter, UserName){
            Key = 'NT29-CH59-PZ24-HY23';
            //SearchTerm  = $('#postcode').val();
            SearchTerm  = $($postcode).val();
            PreferredLanguage = '';
            Filter = '';
            UserName = '';

          //var scriptTag = document.getElementById("PCA6d35cfc188f1451f9cfdf1b5d751a716");
          var scriptTag = $("PCA6d35cfc188f1451f9cfdf1b5d751a716");

          //console.log( $("PCA6d35cfc188f1451f9cfdf1b5d751a716").val() );

          //var headTag = document.getElementsByTagName("head").item(0);
          //console.log( $('head') );
          var headTag = $("head")[0];
          //headTag = headTag[0];
         //console.log( $('headTag') );


          //var headTag = $("html").find('head');
          var strUrl = "";

          //Build the url
          strUrl = "http://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/Find/v1.10/json.ws?";
          strUrl += "&Key=" + encodeURI(Key);
          strUrl += "&SearchTerm=" + encodeURI(SearchTerm);
          strUrl += "&PreferredLanguage=" + encodeURI(PreferredLanguage);
          strUrl += "&Filter=" + encodeURI(Filter);
          strUrl += "&UserName=" + encodeURI(UserName);
          strUrl += "&CallbackFunction=jQuery.PostcodeAnywhere_Interactive_Find_v1_10End";

          //Make the request
          if (scriptTag){
                try{
                      headTag.removeChild(scriptTag);
                }catch (e){
                    //Ignore
                }
            }

          scriptTag = document.createElement("script");
          scriptTag.src = strUrl
          scriptTag.type = "text/javascript";
          scriptTag.id = "PCA6d35cfc188f1451f9cfdf1b5d751a716";
          headTag.appendChild(scriptTag);
        }
    //);
    }); //close jQuery.extend
  
  //$('#return').change(function(){
    jQuery.extend({
        PostcodeAnywhere_Interactive_RetrieveById_v1_10Begin: function(){//Key, Id, PreferredLanguage, UserName){
        //function PostcodeAnywhere_Interactive_RetrieveById_v1_10Begin(Key, Id, PreferredLanguage, UserName){
            
            Id  =   $('#return').val();
            Key =   'NT29-CH59-PZ24-HY23';
            PreferredLanguage   =   '';
            UserName            =   '';

            console.log('Id: ' + Id);

          var scriptTag = document.getElementById("PCAa73f9bc2b60d4e4cbd595512478a3291");
          var headTag = document.getElementsByTagName("head").item(0);
          var strUrl = "";

          //Build the url
          strUrl = "http://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/RetrieveById/v1.10/json.ws?";
          strUrl += "&Key=" + encodeURI(Key);
          strUrl += "&Id=" + encodeURI(Id);
          strUrl += "&PreferredLanguage=" + encodeURI(PreferredLanguage);
          strUrl += "&UserName=" + encodeURI(UserName);
          strUrl += "&CallbackFunction=jQuery.PostcodeAnywhere_Interactive_RetrieveById_v1_10End";

          //Make the request
          if (scriptTag) 
             {
                try
                  {
                      headTag.removeChild(scriptTag);
                  }
                catch (e)
                  {
                      //Ignore
                  }
             }
          scriptTag = document.createElement("script");
          scriptTag.src = strUrl
          scriptTag.type = "text/javascript";
          scriptTag.id = "PCAa73f9bc2b60d4e4cbd595512478a3291";
          headTag.appendChild(scriptTag);
        }
    }); //close jQuery.extend
    
/*$('.target').change(function() {
  alert('Handler for .change() called.');
});*/

    jQuery.extend({
        PostcodeAnywhere_Interactive_RetrieveById_v1_10End: function(response){
          //Test for an error
            if (response.length==1 && typeof(response[0].Error) != 'undefined'){
                //Show the error message
                alert(response[0].Description);
            }else{
                //Check if there were any items found
                if (response.length==0){
                      alert("Sorry, no matching items found");
                }else{
                    var retstring = response[0].Company + "\n" + response[0].Line1 + "\n" + response[0].Line2 + "\n" + response[0].Line3 + "\n" + response[0].PostTown + "\n" + response[0].County + "\n" + response[0].Postcode;

                   $('#return').hide();
                    $('#btnFind').hide();
                   //$('#edit-submitted-personal-address-selected-address').html(retstring);

                   //console.log('RETSTRING: ' + retstring);

                    $line1 = jQuery._fetch_field('line1');
                    $($line1).css('border', '1px solid red').val(response[0].Line1);
                    $line2 = jQuery._fetch_field('line2');
                    $($line2).css('border', '1px solid red').val(response[0].Line2);
                    $line3 = jQuery._fetch_field('line3');
                    $($line3).css('border', '1px solid red').val(response[0].Line3);
                    $line4 = jQuery._fetch_field('line4');
                    $($line4).css('border', '1px solid red').val(response[0].Line4);

                    $town = jQuery._fetch_field('town');
                    $($town).css('border', '1px solid red').val(response[0].PostTown);
                    
               }
            }
        }
    }); //close jQuery.extend

    jQuery.extend({
        PostcodeAnywhere_Interactive_Find_v1_10End: function(responses){
            //Test for an error
            if (responses.length==1 && typeof(responses[0].Error) != 'undefined'){
                //Show the error message
                alert(responses[0].Description);
            }else{
                //Check if there were any items found
                if (responses.length==0){
                    alert("Sorry, no matching items found");
                }else{
                    $('#return').show();
                    
                    $('#return').append(new Option("Select your address"));

                    for (var i=0;i< $(responses).length;i++){
                        //$('#return').append(new Option(responses[i].StreetAddress + ", " + responses[i].Place, responses[i].Id));
                        $option = new Option(responses[i].StreetAddress + ", " + responses[i].Place, responses[i].Id);
                        $('#return').append($option);
                    }
                    //$('#return').bind('change', jQuery.change('NT29-CH59-PZ24-HY23', $('#return').value, '', ''));
                }
            }
        }
    }); //close jQuery.extend

    //$('div[id^="list_"]')
    //$('input[id*="-postcodeanywhere-"]').css('border', '1px solid red');
    //$('#edit-submitted-personal-postcodeanywhere-postcode').css('border', '1px solid green');

    /*******************************
     * dealing with setting up the
     * postcode field as the data
     * entry point to the postcode-
     * anywhere service
     *******************************/

    jQuery.extend({
        activate_postcode_field: function($first) {
            //$check = "-postcodeanywhere-"+$first;
            //$('input[id*="-postcodeanywhere-"]').css('border', '1px solid blue');
            //$postcodeField = $('input[id*="' + $check + '"]');
            /*$line1 = jQuery._fetch_field('line1');
            $($line1).css('border', '1px solid red');

            $line1 = jQuery._fetch_field('line2');
            $($line1).css('border', '1px solid red');

            $line1 = jQuery._fetch_field('line3');
            $($line1).css('border', '1px solid red');

            $line1 = jQuery._fetch_field('line4');
            $($line1).css('border', '1px solid red');

            $line1 = jQuery._fetch_field('town');
            $($line1).css('border', '1px solid red');*/

            $postcodeField = jQuery._fetch_field($first);

            //$postcodeField = jQuery._fetch_field('line1');


            //$postcodeField.css('border', '1px solid blue').val('w11 3by');
            //$button = document.createElement("<input type='button' value='Click to Find' class='btnFind'>");

            $button = "<input type='button' value='Click to Find' id='btnFind'>";
            $($button).insertAfter( $postcodeField );
            $('#btnFind').bind('click', function() {
              jQuery.initiate($postcodeField);
            });

            $select = "<select id='return' onchange='jQuery.PostcodeAnywhere_Interactive_RetrieveById_v1_10Begin()'>";
            $($select).insertAfter( $postcodeField ).hide();

            //$($button).insertAfter( $postcodeField );
        }
    });

    jQuery.extend({
        _fetch_field: function($identifier){
            //$identifier = "-postcodeanywhere-" + $identifier;
            console.log('Looking for: ' + $identifier);
            //$match = $('input[id*=-postcodeanywhere-"' + $identifier + '"]') ? $('input[id$="' + $identifier + '"]') : '';
            return $('input[id*=-postcodeanywhere-"' + $identifier + '"]') ? $('input[id$="' + $identifier + '"]') : '';
            //return $match;
        }
    });

})(jQuery);