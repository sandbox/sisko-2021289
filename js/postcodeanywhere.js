(function ($) {

  $currentIndex   = null;
  $line1          = null;
  $line2          = null;
  $line3          = null;
  $line4          = null;
  $town           = null;
  $nid            = Drupal.settings.webform_postcodeanywhere.nid;
  $postcodeField  = Drupal.settings.webform_postcodeanywhere.postcodeField;
  $postcodeFields = new Array();

  $.each($postcodeField, function($index, $item){
    // console.log("Trying to find: " + Drupal.settings.webform_postcodeanywhere.postcodeField[$index]);
    $postcodeFields[$index]  = jQuery("input[name='" + Drupal.settings.webform_postcodeanywhere.postcodeField[$index] + "']");

    /*****************
     * detect & hide
     *****************/
      ajaxCall('Address Line 1', $index);
      ajaxCall('Address Line 2', $index);
      ajaxCall('Address Line 3', $index);
      ajaxCall('Address Line 4', $index);
      ajaxCall('Town', $index);
  });

    $key = Drupal.settings.webform_postcodeanywhere.key;

    // $settings = array('Post Code', 'Address Line 1', 'Address Line 2', 'Address Line 3', 'Address Line 4', 'Town');

    function ajaxCall($component, $index, $action){
      $action = $action || "hide";
      jQuery.ajax({
        type: 'POST',
        url: 'webform/postcodeanywhere/ajaxcall/' + $component + '/' + $index + '/' + $nid,
        success: function($returned){
          // alert($returned);
          // console.log("Making ajax call for PID#"+$index+ ". Recieved: "+$returned);

          $component = $("[name='" + $returned + "']");//.val('test');
          if($action == 'hide')
            $("[name='" + $returned + "']").parent('div.form-item').hide();
          else
            $($component).parent('div.form-item').fadeIn('slow').find('input').val($action);//.parent('div.form-item').show();                
        }
      });
    }

    jQuery.extend({
        initiate: function($postcode, $index){
          Key               =   $key;
          Filter            =   '';
          UserName          =   '';
          SearchTerm        =   $($postcode).val();
          PreferredLanguage =   '';
          var scriptTag     =   $("PCA6d35cfc188f1451f9cfdf1b5d751a716");
          var headTag       =   $("head")[0];
          var strUrl        =   '';

          //Build the url
          strUrl = "http://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/Find/v1.10/json.ws?";
          strUrl += "&Key=" + encodeURI(Key);
          strUrl += "&SearchTerm=" + encodeURI(SearchTerm);
          strUrl += "&PreferredLanguage=" + encodeURI(PreferredLanguage);
          strUrl += "&Filter=" + encodeURI(Filter);
          strUrl += "&UserName=" + encodeURI(UserName);
          strUrl += "&CallbackFunction=jQuery.PostcodeAnywhere_Interactive_Find_v1_10End";
          // strUrl += "&index=" + $index;

          /***************************************
           * my code which sets the current index 
           * we are working on to use later
           ***************************************/
            $currentIndex = $index;

          //Make the request
          if (scriptTag){
            try{
                  headTag.removeChild(scriptTag);
            }catch (e){
                //Ignore
            }
          }

          scriptTag = document.createElement("script");
          scriptTag.src = strUrl
          scriptTag.type = "text/javascript";
          scriptTag.id = "PCA6d35cfc188f1451f9cfdf1b5d751a716";
          headTag.appendChild(scriptTag);
        }
    }); //close jQuery.extend
  
    jQuery.extend({
      PostcodeAnywhere_Interactive_RetrieveById_v1_10Begin: function($index){
        // alert('testing ... testing : ' + $index);
        Id                  =   $('#return'+$index).val();
        Key                 =   $key;
        UserName            =   '';
        PreferredLanguage   =   '';

          //console.log('Id: ' + Id);

        var scriptTag = document.getElementById("PCAa73f9bc2b60d4e4cbd595512478a3291");
        var headTag   = document.getElementsByTagName("head").item(0);
        var strUrl    = '';

        //Build the url
        strUrl = "http://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/RetrieveById/v1.10/json.ws?";
        strUrl += "&Key=" + encodeURI(Key);
        strUrl += "&Id=" + encodeURI(Id);
        strUrl += "&PreferredLanguage=" + encodeURI(PreferredLanguage);
        strUrl += "&UserName=" + encodeURI(UserName);
        strUrl += "&CallbackFunction=jQuery.PostcodeAnywhere_Interactive_RetrieveById_v1_10End";

        //Make the request
        if (scriptTag){
          try{
            headTag.removeChild(scriptTag);
          }catch (e){
                //Ignore
          }
        }
        scriptTag       = document.createElement("script");
        scriptTag.id    = "PCAa73f9bc2b60d4e4cbd595512478a3291";
        scriptTag.src   = strUrl
        scriptTag.type  = "text/javascript";
        headTag.appendChild(scriptTag);
      }
    }); //close jQuery.extend

    jQuery.extend({
        PostcodeAnywhere_Interactive_RetrieveById_v1_10End: function(response){
          //Test for an error
            if (response.length==1 && typeof(response[0].Error) != 'undefined'){
                //Show the error message
                alert(response[0].Description);
            }else{
                //Check if there were any items found
                if (response.length==0){
                      alert("Sorry, no matching items found");
                }else{
                  var retstring = '';
                  var retstring = response[0].Company + "\n" + response[0].Line1 + "\n" + response[0].Line2 + "\n" + response[0].Line3 + "\n" + response[0].PostTown + "\n" + response[0].County + "\n" + response[0].Postcode;

                  $('#return'+$currentIndex).hide();
                  // $('#btnFind'+$currentIndex).hide();

                  //$line1 = $('#edit-submitted-personal-address-line1');
                  // $line1 = 
                  ajaxCall('Address Line 1', $currentIndex, response[0].Line1);
                  // $($line1).fadeIn('slow').find('input').val(response[0].Line1);

                  //$line2 = $('#edit-submitted-personal-address-line2');
                  ajaxCall('Address Line 2', $currentIndex, response[0].Line2);
                  // $($line2).fadeIn('slow').find('input').val(response[0].Line2);

                  //$line3 = $('#edit-submitted-personal-address-line3');
                  ajaxCall('Address Line 3', $currentIndex, response[0].Line3);
                  // $($line3).fadeIn('slow').find('input').val(response[0].Line3);

                  //$line4 = $('#edit-submitted-personal-address-line4');
                  ajaxCall('Address Line 4', $currentIndex, response[0].Line4);
                  // $($line4).fadeIn('slow').find('input').val(response[0].Line4);

                  //$town = $('#edit-submitted-personal-address-town');
                  ajaxCall('Town', $currentIndex, response[0].PostTown);
                  // $($town).fadeIn('slow').find('input').val(response[0].PostTown);

               }
            }
        }
    }); //close jQuery.extend

    jQuery.extend({
        PostcodeAnywhere_Interactive_Find_v1_10End: function(responses){
            //Test for an error
            if (responses.length==1 && typeof(responses[0].Error) != 'undefined'){
                //Show the error message
                alert(responses[0].Description);
            }else{
                //Check if there were any items found
                if (responses.length==0){
                    alert("Sorry, no matching items found");
                }else{
                    $('#return'+$currentIndex).empty().show();
                    
                    $('#return'+$currentIndex).append(new Option("Select your address"));

                    for (var i=0;i< $(responses).length;i++){
                        $option = new Option(responses[i].StreetAddress + ", " + responses[i].Place, responses[i].Id);
                        $('#return'+$currentIndex).append($option);
                    }
                }
            }
        }
    }); //close jQuery.extend

    jQuery.extend({
        activate_postcode_field: function() {

            $.each($postcodeFields, function($index, $item){
              
              $btnFind = 'btnFind'+$index;
              $button = "<input type='button' value='Click to Find' id='"+ $btnFind + "'>";
              $($button).insertAfter( $item );

              /********************
               * button binding
               ********************/
                $btnFind = '#btnFind'+$index;
                $($btnFind).bind('click', function() {
                    $($item).css('background', 'pink');
                    jQuery.initiate($item, $index);
                });

                /**********************
                 * select list binding
                 **********************/
                $returnn = 'return'+$index;
                $select = "<select id='"+$returnn+"' onchange='jQuery.PostcodeAnywhere_Interactive_RetrieveById_v1_10Begin("+$index+")'>";
                $($select).insertAfter( $item ).hide();

            });
        }
    });

      $("input").keypress(function (evt) {
        //Deterime where our character code is coming from within the event
        var charCode = evt.charCode || evt.keyCode;
        if (charCode  == 13) { //Enter key's keycode
          //console.log('Detected ENTER keypress');
          return false;
        }
      });

      /*$($postcodeField).keypress(function (evt) {
        //Deterime where our character code is coming from within the event
        var charCode = evt.charCode || evt.keyCode;
        if (charCode  == 13) { //Enter key's keycode
          //console.log('OK to allow postcodeField ENTER keypress');
          jQuery.initiate($postcodeField);
          //return false;
        }
      });*/

})(jQuery);